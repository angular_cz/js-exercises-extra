import React from 'react';
import Header from './Header';
import Row from './Row';

var UserTable = React.createClass({
  propTypes: {
    title: React.PropTypes.string.isRequired,
    users: React.PropTypes.array
  },

  getDefaultProps: function() {
    return {
      users: []
    }
  },

  getInitialState: function() {
    return {
      users: this.props.users
    }
  },

  render: function() {
    var title = this.props.title ? <h2>{this.props.title}</h2> : "";

    var header = this.renderHeader_();

    var rows = this.state.users.map(this.renderRow_);

    return (
        <div>
          {title}
          <button className="btn btn-info" onClick={this.renewData}>Načti data znovu</button>
          <table className="table">
            <thead>
            {/* TODO 1.2 použití komponenty Header v UserTable */}
            {header}
            </thead>
            <tbody>
            {rows}
            </tbody>
          </table>
        </div>
    );
  },

  renderHeader_: function() {
    return <tr>
      <th>Jméno</th>
      <th>Email</th>
      <th>Akce</th>
    </tr>;
  },

  renderRow_: function(user) {
    // TODO 2.1 - komponenta Row - použití
    // TODO 2.2 - komponenta Row - předání atributu user
    // TODO 2.3 - komponenta Row - předání atributu key

    // TODO 3.4 - tlačítko smazat - použití

    return <tr>
      <td>{user.name}</td>
      <td>{user.email}</td>
      <td>
        <button className="btn btn-danger">Delete</button>
      </td>
    </tr>;
  },

  renewData: function() {
    //  TODO 4.1 - obnovení dat (změna stavu komponenty)
  },

  onDelete: function(userToDelete) {

    var newUsers = this.state.users.filter(function(user) {
      return user !== userToDelete;
    });

    this.setState({
      users: newUsers
    });

  },
});

export default UserTable;