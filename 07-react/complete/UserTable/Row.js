import React from 'react';

var Row = React.createClass({
  propTypes: {
    user: React.PropTypes.shape({
      name: React.PropTypes.string,
      email: React.PropTypes.string
    }).isRequired,

    // TODO 3.1 - tlačítko smazat - rozšíření rozhraní komponenty
    onDelete: React.PropTypes.func
  },

  getDefaultProps: function() {
    return {
      // TODO 3.3 - tlačítko smazat - výchozí handler
      onDelete: function() {}
    };
  },

  render: function() {
    var user = this.props.user;

    if (!user) {
      return null;
    }

    return <tr>
      <td>{user.name}</td>
      <td>{user.email}</td>
      <td>
        {/* TODO 3.2 - tlačítko smazat - volání handleru */}
        <button onClick={this.handleClick} className="btn btn-danger">Smazat</button>
      </td>
    </tr>;
  },

  handleClick: function(event) {
    console.log("handleClick")
    this.props.onDelete(this.props.user);
  }

});

export default Row;