angular.module('userApp')
  .directive('userForm', function ($q) {

    function UserFormController() {

      this.isCancelVisible = function () {
        return this.hideCancel === undefined;
      }
      this.save = function () {
        var promise = $q.when(this.onSave({user: this.user}));
        promise.then(this.clear_.bind(this));
      }

      this.clear_ = function () {
        this.user = {};

        this.form.$setPristine();
        this.form.$setUntouched();
      }
    }

    return {
      restrict: 'E',
      templateUrl: 'user-edit.html',

      scope: {
        user: '=',
        onSave: '&',
        hideCancel: '@'
      },

      controller: UserFormController,
      controllerAs: 'userEdit',
      bindToController: true,
    }
  })
