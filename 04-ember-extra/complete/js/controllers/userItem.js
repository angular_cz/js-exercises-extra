App.UserItemController = Ember.Controller.extend({
  changeArchived: Ember.observer('model.archived', function () {
    var user = this.get('model');
    if (user.get('hasDirtyAttributes')) {
      user.save();
    }
  }),

  actions: {
    remove: function () {
      var user = this.get('model');
      user.destroyRecord();
    }
  }
});
