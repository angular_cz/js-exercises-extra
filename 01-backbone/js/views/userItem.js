var app = app || {};

app.UserItemView = Backbone.View.extend({
  tagName: 'tr',
  template: _.template($('#user-template').html()),

  initialize: function () {
    this.listenTo(this.model, 'change', this.render);

    // TODO 2.3 - Reagujte na odstranění modelu
  },

  events: {
    'change input[name=archived]': 'setArchived',

    // TODO 2.1 - Zpracujte akci kliknutí
  },

  // TODO 2.2 - Odstraňte záznam

  setArchived: function (e) {
    var checked = this.$('input[name=archived]').is(':checked');
    this.model.setArchived(checked);
  },

  render: function () {
    this.$el.html(this.template(this.model.toJSON()));

    this.$el.removeClass('archived');

    if (this.model.get('archived')) {
      this.$el.addClass('archived');
    }
    return this;
  }
});
