app.User = Backbone.Model.extend({
  urlRoot: '/api/users',

  validate: function (attrs) {
    var errors = null;
    if (!attrs.name) {
      errors = errors || {};
      errors.name = 'Musíte zadat jméno.';
    }
    if (!attrs.email) {
      errors = errors || {};
      errors.email = 'Musíte zadat email.';
    } else {
      var emailPattern = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      if (!emailPattern.test(attrs.email)) {
        errors = errors || {};
        errors.email = 'Email není platný.';
      }
    }

    return errors;
  },

  defaults: {
    name: '',
    email: '',
    archived: false
  },
  setArchived: function (archived) {
    this.set({'archived': archived});
    this.save();
  }
});

