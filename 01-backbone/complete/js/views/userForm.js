var app = app || {};

app.UserFormView = Backbone.View.extend({
  template: _.template($('#user-form-template').html()),

  events: {
    submit: 'save'
  },

  initialize: function () {
    this.listenTo(this.model, 'change', this.render);
    this.listenTo(this.model, 'invalid', this.invalid);
  },

  hideCancel: function () {
    this.isCancelHidden = true;
  },

  save: function (e) {
    e.preventDefault();
    var data = {
      name: this.$('input[name=name]').val(),
      email: this.$('input[name=email]').val(),
      archived: this.$('input[name=archived]').is(':checked')
    };

    this.model.set(data);
    if (!this.model.isValid()) {
      return;
    }

    this.trigger("save", this.model);
  },

  invalid: function (model, error) {
    if (error.name) {
      this.$('#name-validation').show();
      this.$('#name-validation').html(error.name);
      this.$('input[name=name]').addClass('invalid');
    }
    if (error.email) {
      this.$('#email-validation').show();
      this.$('#email-validation').html(error.email);
      this.$('input[name=email]').addClass('invalid');
    }
  },

  render: function () {
    this.$el.html(this.template(this.model.toJSON()));

    this.$('.validation').hide();
    this.$('input').removeClass('invalid');

    if (this.isCancelHidden) {
      this.$('#cancel-button').hide();
    }
  }
});

