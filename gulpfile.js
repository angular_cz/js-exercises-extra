var gulp = require('gulp');
var connect = require('connect');
var serveStatic = require('serve-static');
var proxy = require('proxy-middleware');
var url = require('url');


var config = {
  httpServer: {
    host: 'localhost',
    port: 8000,
    lrPort: 35730
  },
  proxy:'http://localhost:8080/no-session/'
};

gulp.task('connect', function () {

  var argv = require('yargs').argv;
  console.log("Js-exercises - www.angular.cz");

  if(argv.proxy) {
    config.proxy = argv.proxy;
  }

  console.log("Api proxy: " + config.proxy);

  console.log("Http-server running on http://localhost:" + config.httpServer.port);

  var app = connect();
  app.use(serveStatic('./'));
  app.listen(config.httpServer.port);

  app.use('/api', proxy(url.parse(config.proxy)));
});

gulp.task('default', ['connect']);