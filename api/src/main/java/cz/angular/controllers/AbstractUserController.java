package cz.angular.controllers;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;

import cz.angular.user.User;
import cz.angular.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

public class AbstractUserController {

  protected UserService userService;

  @RequestMapping(value = "/users", method= RequestMethod.GET)
  public Collection<User> getUsers() {
    return userService.getUsers();
  }

  @RequestMapping(value = "/users", method= RequestMethod.POST)
  public User createUser(@RequestBody User user) {
    return userService.createUser(user);
  }

  @RequestMapping(value = "/users/{id}", method= RequestMethod.GET)
  public User getUser(@PathVariable Long id) {
    return userService.getUser(id);
  }

  @RequestMapping(value = "/users/{id}", method= RequestMethod.DELETE)
  public void removeUser(@PathVariable Long id) {
    userService.removeUser(id);
  }

  @RequestMapping(value = "/users/{id}", method = {RequestMethod.POST, RequestMethod.PUT})
  public User updateUser(@PathVariable Long id, @RequestBody User user) {
    return userService.updateUser(id, user);
  }

}