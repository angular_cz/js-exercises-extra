package cz.angular.controllers.wrapped;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by vita on 02.08.15.
 */
@JsonSerialize
public class Empty {
}
