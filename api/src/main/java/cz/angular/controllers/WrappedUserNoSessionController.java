package cz.angular.controllers;

import cz.angular.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= "/no-session/wrapped")
public class WrappedUserNoSessionController extends AbstractWrappedUserController {


  @Autowired
  public WrappedUserNoSessionController(@Qualifier("userNoSessionService") UserService userService) {
    this.userService = userService;
  }


}