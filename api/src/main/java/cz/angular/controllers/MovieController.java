package cz.angular.controllers;

import cz.angular.user.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by vita on 20.08.15.
 */
@RestController()
public class MovieController {

  @RequestMapping(value = {"/movies", "/no-session/movies"}, method = RequestMethod.GET)
  public Collection<Movie> getMovies() {

    Double ceil = Math.ceil(Math.random() * 1000);

    try {
      Thread.sleep(ceil.intValue());
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    List<Movie> movies = new ArrayList<Movie>();
    movies.add(new Movie(1, "Vykoupení z věznice Shawshank", 1994, 95.2));
    movies.add(new Movie(2, "Forrest Gump", 1994, 94.7));
    movies.add(new Movie(3, "Přelet nad kukaččím hnízdem", 1975, 92.8));
    movies.add(new Movie(4, "Zelená míle", 1999, 92.7));
    movies.add(new Movie(5, "Schindlerův seznam", 1993, 92.3));
    movies.add(new Movie(6, "Sedm", 1995, 92.3));
    movies.add(new Movie(7, "Kmotr", 1972, 92.0));
    movies.add(new Movie(8, "Dvanáct rozhněvaných mužů", 1957, 91.4));
    movies.add(new Movie(9, "Nedotknutelní", 2011, 91.3));
    movies.add(new Movie(10, "Kmotr II", 1974, 91.3));

    return movies;
  }


  private class Movie {
    private int rank;
    private String name;
    private int year;
    private double rating;

    public int getRank() {
      return rank;
    }

    public void setRank(int rank) {
      this.rank = rank;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public int getYear() {
      return year;
    }

    public void setYear(int year) {
      this.year = year;
    }

    public double getRating() {
      return rating;
    }

    public void setRating(double rating) {
      this.rating = rating;
    }

    public Movie(int rank, String name, int year, double rating) {
      this.rank = rank;
      this.name = name;
      this.year = year;
      this.rating = rating;
    }
  }
}
