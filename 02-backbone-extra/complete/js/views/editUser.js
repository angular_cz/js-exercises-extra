var app = app || {};

app.EditUserView = Backbone.View.extend({
  template: _.template($('#edit-user-template').html()),

  render: function () {
    this.$el.html(this.template());

    var formView = new app.UserFormView({
        el: this.$('#edit-form'),
        model: this.model
      }
    );

    formView.on('save', function (model) {
      model.save()
        .then(function () {
          Backbone.history.navigate('users', {trigger: true});
        });
    });

    formView.render();
  }
});
