var app = app || {};

app.UserItemView = Backbone.View.extend({
  tagName: 'tr',
  template: _.template($('#user-template').html()),

  initialize: function () {
    this.model.on('change', this.render, this);
    this.model.on('destroy', this.remove, this);
  },

  events: {
    'change input[name=archived]': 'setArchived',
    'click .remove': 'destroy'
  },

  destroy: function () {
    this.model.destroy();
  },

  setArchived: function (e) {
    var checked = this.$('input[name=archived]').is(':checked');
    this.model.setArchived(checked);
  },

  render: function () {
    this.$el.html(this.template(this.model.toJSON()));

    this.$el.removeClass('archived');

    if (this.model.get('archived')) {
      this.$el.addClass('archived');
    }
    return this;
  }
});
