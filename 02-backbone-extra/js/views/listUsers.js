var app = app || {};

app.ListUsersView = Backbone.View.extend({
  // TODO 1 - načtěte šablonu z elementu s id list-users-template
  template: _.template(''),

  initialize: function () {
    //TODO 8.3 - Reagujte na změnu řazení kolekce
    this.listenTo(this.collection, 'add', this.addOne);
    this.listenTo(this.collection, 'reset', this.addAll);
  },

  render: function () {
    this.$el.html(this.template());
    this.$list = this.$('#user-list');

    // TODO 8.1 - Zavolejte renderování formuláře
  },

  renderForm: function () {
    var formView = new app.UserFormView({
        el: this.$('#create-form'),
        model: new app.User()
      }
    );

    formView.hideCancel();
    formView.on('save', function (model) {

      // TODO 8.2 - Přidejte data modelu do kolekce

      model.set({
        name: '',
        email: '',
        archived: false
      });
    }.bind(this));

    formView.render();
  },

  addAll: function () {
    this.$list.html('');
    this.collection.forEach(this.addOne, this);
  },
  addOne: function (item) {
    var userView = new app.UserItemView({model: item});
    userView.render();

    this.$list.append(userView.el);
  }
});
