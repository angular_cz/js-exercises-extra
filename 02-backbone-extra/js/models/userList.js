var app = app || {};

app.UserList = Backbone.Collection.extend({

  //TODO 2.1 - Definujte url kolekce
  model: app.User,

  comparator: function (item) {
    return item.get('name').toLowerCase();
  }
});