angular.module('userApp')
  .factory('User', function ($resource) {
    return $resource('/api/users/:id', {'id': '@id'});
  });