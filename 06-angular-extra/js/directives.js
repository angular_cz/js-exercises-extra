angular.module('userApp')
  .directive('userForm', function () {

    function UserFormController() {

      this.isCancelVisible = function () {
        return this.hideCancel === undefined;
      }

      this.save = function () {
        var promise = $q.when(this.onSave({user: this.user}));
        promise.then(this.clear_.bind(this));
      }

      this.clear_ = function () {
        this.user = {};

        this.form.$setPristine();
        this.form.$setUntouched();
      }
    }

    return {
      restrict: 'E',

      scope: {
        // TODO 4.2 - Přidejte attribut pro model uživatele

        onSave: '&',
        hideCancel: '@'
      },

      templateUrl: 'user-edit.html',

      controller: UserFormController,
      controllerAs: 'userEdit',
      bindToController: true
    }
  })
