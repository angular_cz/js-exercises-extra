App.UsersController = Ember.Controller.extend({
  sortProperties: ['name'],
  sortedUsers: Ember.computed.sort('model', 'sortProperties'),

  init: function () {
    this.set('newUser', Ember.Object.create());
  },

  actions: {
    createUser: function (newUser) {
      var user = this.store.createRecord('user', {
        name:  newUser.get('name'),
        email: newUser.get('email'),
        archived : newUser.get('archived')
      });

      user.save();
      this.init();
    }
  }
});